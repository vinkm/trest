from typing import Any, Dict

from flask import Flask, request

from .endpoint import Endpoint
from .error import bad_request, not_found
from .trest_flask import TRESTFlask


class TRESTServer:
    def __init__(self) -> None:
        self.app: Flask = None
        self.routes: Dict[str, Endpoint] = {}

    def api(self) -> Any:
        if not request.json:
            bad_request('Calls to /trest/api need to have a Content-Type: application/json and a json body')
        if request.method == 'POST':
            route: str = request.json('route')
            if not route:
                return bad_request('I need a route to create/amend an Endpoint')
            endpoint: Endpoint = self.routes.get(route, Endpoint(request.json))
            not_valid: str = endpoint.validate()
            if not_valid:
                bad_request(not_valid)
            self.routes[endpoint.route] = endpoint
            self.app.add_url_rule(endpoint.route, view_func = endpoint.handle, methods = endpoint.methods)
            return 'Endpoint for %s created' % endpoint.route
        elif request.method == 'DELETE':
            route: str = request.json('route')
            if not route:
                bad_request('Please tell me the route of the Endpoint you want deleted')
            if route not in self.routes:
                not_found('Could not find an endpoint for route %s' % route)
            self.routes[route].delete()
            return 'Endpoint %s \'deleted\'' % route
        bad_request('GET /trest/api is not implemented yet')

    def get_app(self, _test_config: Dict[str, Any] = None) -> Flask:
        if not self.app:
            self.app = TRESTFlask(__name__, instance_relative_config = True)
            self.app.config.from_mapping(SECRET_KEY = 'dev')
            if _test_config:
                self.app.config.from_mapping(_test_config)
            else:
                self.app.config.from_pyfile('trest.cfg', silent = True)
            self.app.add_url_rule('/trest/api', 'api', self.api, methods = ['GET', 'POST', 'DELETE'])
        return self.app
