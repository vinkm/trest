from flask import make_response, jsonify
from werkzeug.exceptions import HTTPException


class TRESTError(HTTPException):
    def __init__(self, _code: int, _error = None):
        if _error:
            super().__init__(response = make_response(jsonify({'error': _error}), _code, {'Content-Type': 'application/json'}))
        else:
            super().__init__()
            self.code = _code


def bad_request(_error = None) -> None:
    raise TRESTError(400, _error)


def not_found(_error = None) -> None:
    raise TRESTError(404, _error)
