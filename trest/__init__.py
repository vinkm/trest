from typing import Any, Dict

from flask import Flask

from .server import TRESTServer

trest_server: TRESTServer = TRESTServer()


def create_app(_test_config: Dict[str, Any] = None) -> Flask:
    return trest_server.get_app(_test_config)
