from typing import Any, Dict, List
from uuid import uuid4

from .misc import ContentTypes


class Behaviour:
    def __init__(self, _json: Dict[str, Any]) -> None:
        self.uuid = uuid4()
        self.content_type: str = _json.get('contentType', ContentTypes.json)
        self.methods: List[str] = [_method.upper() for _method in _json.get('methods', ['GET'])]
        count: Any = _json.get('count')
        self.count: int = None if not count else int(count)
        response = _json.get('response')
        if not response or isinstance(response, str):
            self.content_type = ContentTypes.text
            self.code: int = 200
            self.body: str = '' if not response else response
        else:
            self.code: int = int(response.get('code', 200))
            self.body: str = response.get('body', '')
            if not self.body:
                self.content_type = ContentTypes.text

    def response(self, **kwargs) -> Any:
        if self.count:
            self.count -= 1
        body: str = self.body
        for key, arg in kwargs.items():
            repl: str = '&%s' % key
            if repl in body:
                body = body.replace(repl, arg)
        return body

    def valid(self) -> bool:
        return self.count is None or self.count > 0
