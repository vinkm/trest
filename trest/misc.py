class ContentTypes:
    all: str = '*'
    json: str = 'application/json'
    text: str = 'plain/text'
    xml: str = 'application/xml'
