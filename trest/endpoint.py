from threading import Lock
from typing import Any, Dict, List, Optional

from flask import jsonify, make_response

from .misc import ContentTypes
from .endpoint_behaviour import Behaviour
from .error import not_found


class EndpointResponse:
    def __init__(self, _code: int, _body: Any, _content_type: str) -> None:
        self.code: int = _code
        self.body: Any = _body
        self.content_type: str = _content_type

    def to_flask_response(self) -> Any:
        if self.content_type == ContentTypes.json:
            return make_response(jsonify(self.body), self.code, {'Content-Type': self.content_type})
        return make_response(self.body, self.code, {'Content-Type': self.content_type})


class Endpoint:
    def __init__(self, _json: Dict[str, Any]) -> None:
        self.route: str = _json.get('route')
        if self.route and not self.route.startswith('/'):
            self.route = '/%s' % self.route
        self.behaviours: List[Behaviour] = []
        self.lock: Lock = Lock()

    def validate(self) -> Optional[str]:
        if not self.route:
            return 'Invalid Endpoint with no route'
        if self.route.startswith('/trest'):
            return 'You cannot use routes that start with /trest, that is reserved for me'
        return None

    def add_behaviour(self, _json: Dict[str, Any]):
        self.behaviours.append(Behaviour(_json))

    def handle(self, **kwargs):
        if not self.behaviours:
            not_found()
        config: Behaviour = self.behaviours[-1]
        if config.code == 404 or not config.valid():
            not_found()
        response = EndpointResponse(config.code, config.response(**kwargs), config.content_type)
        if not config.valid():
            self.lock.acquire()
            try:
                if self.behaviours and self.behaviours[-1].uuid == config.uuid:
                    self.behaviours.pop()
            finally:
                self.lock.release()
        return response

    def pop(self):
        if self.behaviours:
            self.lock.acquire()
            try:
                if self.behaviours:
                    self.behaviours.pop()
            finally:
                self.lock.release()

    def clear(self):
        if self.behaviours:
            self.lock.acquire()
            try:
                if self.behaviours:
                    self.behaviours.clear()
            finally:
                self.lock.release()
        pass
