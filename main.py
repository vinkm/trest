from flask import Flask

from trest import create_app

app: Flask = create_app()

app.run(debug = True)
