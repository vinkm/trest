from typing import Any, Dict

import pytest

from trest.endpoint import Endpoint, EndpointResponse
from trest.error import TRESTError


def assert_response(_response: EndpointResponse, _code: int = None, _type: str = None, _body: Any = None) -> None:
    if _code:
        assert _code == _response.code
    if _type:
        assert _type == _response.content_type
    if _body:
        assert _body == _response.body


def _config(**kwargs) -> Dict[str, Any]:
    cfg: Dict[str, Any] = {
        'route': '/test'
    }
    for _key in kwargs:
        cfg[_key] = kwargs[_key]
    return cfg


def _endpoint(**kwargs) -> Endpoint:
    cfg: Dict[str, Any] = _config(**kwargs)
    endpoint: Endpoint = Endpoint(cfg)
    endpoint.add_behaviour(cfg)
    return endpoint


def assert_error(_endpoint: Endpoint, _code: int = None):
    with pytest.raises(TRESTError) as e:
        _endpoint.handle()

    assert not _code or _code == e.value.code


def test_should_be_invalid_if_no_route_present():
    assert 'Invalid Endpoint with no route' == Endpoint({}).validate()


def test_should_be_invalid_if_trest_route_present():
    assert 'You cannot use routes that start with /trest, that is reserved for me' == Endpoint({'route': '/trest'}).validate()


def test_should_default_to_404_if_no_behaviour_provided():
    assert_error(Endpoint({'route': '/test'}), 404)


def test_should_override_behaviour_on_subsequent_upserts():
    # create endpoint with a behaviour
    first_body = 'first behaviour'
    endpoint: Endpoint = _endpoint(response = {'body': first_body})
    assert_response(endpoint.handle(), _code = 200, _body = first_body)
    # replace behaviour with a second one
    second_body = 'second behaviour'
    second_code = 301
    endpoint.add_behaviour(_config(response = {'body': second_body, 'code': second_code}))
    # replace behaviour with a third one
    assert_response(endpoint.handle(), _code = second_code, _body = second_body)
    third_body = 'third behaviour'
    third_code = 201
    endpoint.add_behaviour(_config(response = {'body': third_body, 'code': third_code}))
    assert_response(endpoint.handle(), _code = third_code, _body = third_body)


def test_should_only_return_response_x_times_and_then_404():
    # create an endpoint that expires after `count` requests get served
    body = 'kilroy was here'
    endpoint: Endpoint = _endpoint(response = {'body': body}, count = 4)
    for i in range(4):
        assert_response(endpoint.handle(), _code = 200, _body = body)
    assert_error(endpoint, 404)


def test_should_consider_config_with_no_count_as_default_and_return_to_it_when_subsequent_config_expires():
    # create an endpoint with a default (no count) behaviour
    body = 'kilroy was here'
    endpoint: Endpoint = _endpoint(response = {'body': body})
    for i in range(10):
        assert_response(endpoint.handle(), _code = 200, _body = body)
    # replace it with a temporary one (count of 5)
    alt_code = 201
    alt_body = 'joy oh joy, I got here before kilroy'
    endpoint.add_behaviour(_config(response = {'body': alt_body, 'code': alt_code}, count = 5))
    for i in range(5):
        assert_response(endpoint.handle(), _code = alt_code, _body = alt_body)
    # the temp behaviour has expired, return to the default
    assert_response(endpoint.handle(), _code = 200, _body = body)


def test_should_handle_multiple_interleaved_timed_config_upserts():
    # create an endpoint with a default (no count) behaviour
    body = 'kilroy was here'
    endpoint: Endpoint = _endpoint(response = {'body': body})
    for i in range(10):
        assert_response(endpoint.handle(), _code = 200, _body = body)
    # replace it with a temporary one (count of 5)
    alt_code = 201
    alt_body = 'joy oh joy, I got here before kilroy'
    endpoint.add_behaviour(_config(response = {'body': alt_body, 'code': alt_code}, count = 5))
    # get 2 responses
    for i in range(2):
        assert_response(endpoint.handle(), _code = alt_code, _body = alt_body)
    # replace it with another temporary one (count of 10)
    alt_code2 = 205
    alt_body2 = 'omg, they killed kenny'
    endpoint.add_behaviour(_config(response = {'body': alt_body2, 'code': alt_code2}, count = 10))
    # check 10 responses
    for i in range(10):
        assert_response(endpoint.handle(), _code = alt_code2, _body = alt_body2)
    # check the remaining 3 from the previous one
    for i in range(3):
        assert_response(endpoint.handle(), _code = alt_code, _body = alt_body)
    # and now return to the default
    assert_response(endpoint.handle(), _code = 200, _body = body)


def test_permanent_behaviours_can_only_be_removed_by_user_interaction():
    # create an endpoint with a default (no count) behaviour
    body = 'kilroy was here'
    endpoint: Endpoint = _endpoint(response = {'body': body})
    for i in range(10):
        assert_response(endpoint.handle(), _code = 200, _body = body)
    # replace it with a temporary one (count of 5)
    alt_code = 201
    alt_body = 'joy oh joy, I got here before kilroy'
    endpoint.add_behaviour(_config(response = {'body': alt_body, 'code': alt_code}, count = 5))
    # get 2 responses
    for i in range(2):
        assert_response(endpoint.handle(), _code = alt_code, _body = alt_body)
    # replace it with another temporary one (count of 10)
    alt_code2 = 205
    alt_body2 = 'omg, they killed kenny'
    endpoint.add_behaviour(_config(response = {'body': alt_body2, 'code': alt_code2}, count = 10))
    # check 6 responses
    for i in range(6):
        assert_response(endpoint.handle(), _code = alt_code2, _body = alt_body2)
    # now introduce new permanent behaviour (new default
    bob_code = 301
    bob_body = 'bob was here'
    endpoint.add_behaviour(_config(response = {'body': bob_body, 'code': bob_code}))
    # which now becomes a fixture
    for i in range(10):
        assert_response(endpoint.handle(), _code = bob_code, _body = bob_body)
    # until we remove it
    endpoint.pop()
    # at which point we resume the previous behaviours
    for i in range(4):
        assert_response(endpoint.handle(), _code = alt_code2, _body = alt_body2)
    for i in range(3):
        assert_response(endpoint.handle(), _code = alt_code, _body = alt_body)

    assert_response(endpoint.handle(), _code = 200, _body = body)
    # and we then remove the current 'default'
    endpoint.pop()
    # at which point we only return 404
    assert_error(endpoint, 404)


def test_should_remove_all_behaviours_in_one_fell_swoop():
    # create an endpoint with a default (no count) behaviour
    body = 'kilroy was here'
    endpoint: Endpoint = _endpoint(response = {'body': body})
    for i in range(10):
        assert_response(endpoint.handle(), _code = 200, _body = body)
    # replace it with a temporary one (count of 5)
    alt_code = 201
    alt_body = 'joy oh joy, I got here before kilroy'
    endpoint.add_behaviour(_config(response = {'body': alt_body, 'code': alt_code}, count = 5))
    # get 2 responses
    for i in range(2):
        assert_response(endpoint.handle(), _code = alt_code, _body = alt_body)
    # replace it with another temporary one (count of 10)
    alt_code2 = 205
    alt_body2 = 'omg, they killed kenny'
    endpoint.add_behaviour(_config(response = {'body': alt_body2, 'code': alt_code2}, count = 10))
    # check 6 responses
    for i in range(6):
        assert_response(endpoint.handle(), _code = alt_code2, _body = alt_body2)
    # and now delete them all
    endpoint.clear()
    # at which point we only return 404
    assert_error(endpoint, 404)
