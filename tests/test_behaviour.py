from typing import Any, Dict

from trest.misc import ContentTypes
from trest.endpoint_behaviour import Behaviour


def assert_response(_behaviour: Behaviour, _code: int = None, _type: str = None, _body: Any = None) -> None:
    if _code:
        assert _code == _behaviour.code
    if _type:
        assert _type == _behaviour.content_type
    if _body:
        assert _body == _behaviour.body


def _behaviour(**kwargs) -> Behaviour:
    cfg: Dict[str, Any] = {
        'route': '/test'
    }
    for _key in kwargs:
        cfg[_key] = kwargs[_key]
    return Behaviour(cfg)


def test_should_default_to_plain_text_if_string_response_provided():
    body = 'test'
    behaviour: Behaviour = _behaviour(response = body)
    assert_response(behaviour, _code = 200, _type = ContentTypes.text, _body = body)


def test_should_default_to_plain_text_if_empty_body():
    code = 200
    behaviour: Behaviour = _behaviour(response = {'code': code, 'body': ''})
    assert_response(behaviour, _code = code, _type = ContentTypes.text, _body = '')


def test_should_default_to_plain_text_if_no_body():
    code = 200
    behaviour: Behaviour = _behaviour(response = {'code': code})
    assert_response(behaviour, _code = code, _type = ContentTypes.text)
    assert not behaviour.body


def test_should_default_to_200_if_missing_code():
    behaviour: Behaviour = _behaviour(response = {'body': 'bob'})
    assert_response(behaviour, _code = 200, _body = 'bob')


def test_should_return_given_content_type():
    body = '''
<?xml version="1.0" encoding="UTF-8"?>
<note>
    <to>Tove</to>
    <from>Jani</from>
    <heading>Reminder</heading>
    <body>Don't forget me this weekend!</body>
</note>'''
    behaviour: Behaviour = _behaviour(contentType = 'application/xml', response = {'code': 200, 'body': body})
    assert_response(behaviour, _type = ContentTypes.xml, _body = body)


def test_should_default_to_json_type_if_valid_body_is_provided():
    behaviour: Behaviour = _behaviour(response = {'code': 204, 'body': 'hello'})
    assert_response(behaviour, _code = 204, _type = ContentTypes.json)


def test_should_only_return_response_x_times_and_then_become_invalid():
    # create a behaviour that expires after `count` requests get served
    body = 'kilroy was here'
    behaviour: Behaviour = _behaviour(response = {'body': body}, count = 4)
    for i in range(4):
        assert behaviour.valid() is True
        assert body == behaviour.response()
    assert behaviour.valid() is False


def test_should_return_indefinitely_if_no_count_provided():
    body = 'kilroy was here'
    behaviour: Behaviour = _behaviour(response = {'body': body})
    for i in range(100):
        assert behaviour.valid() is True
        assert body == behaviour.response()
    assert behaviour.valid() is True


def test_should_replace_amped_values_with_corresponding_kwargs_ignoring_non_matches():
    body: str = '{"name": "&name", "action": "was here"}'
    behaviour: Behaviour = _behaviour(response = {'body': body})
    name: str = "kilroy"
    expected: str = '{"name": "kilroy", "action": "was here"}'
    assert expected == behaviour.response(name = name)
