# tREST - a test REST API server

... based on Flask

# IMPORTANT: To allow dynamic routes to be added to flask, I have had to bypass one of it's security features. *NEVER* use this in a production environment
Seriously, though, it's a test server (it says so in the name), why would you even think of using it production? 

