from setuptools import find_packages, setup

setup(
    name = 'trest',
    version = '0.0.1',
    packages = find_packages(),
    install_requires = [
        'atomicwrites',
        'attrs',
        'Click',
        'Flask',
        'itsdangerous',
        'Jinja2',
        'MarkupSafe',
        'more-itertools',
        'pluggy',
        'py',
        'pytest',
        'six',
        'Werkzeug'
    ]
)


