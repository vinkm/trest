# plugins
  + auth:
    - type:
      - json
      - form
    - source:
      - any
      - provided list
      - file
  + idgen:
    - generate an id on each (valid) call
  + idtrack:
    - keep track of requests for a given id and return consistent responses
  + standard:
    - return a given response
  + playbook

# set endpoints
  + POST /test-api/endpoint
    - api path
    - desired response
    - times:
      - if provided, will return a given response X number of times, before automatically removing this endpoint
        - if there is already an endpoint serving this path, the response of said endpoint will be changed for X times and then reverted to its previous style
      - if -1 (or absent), it the api will be registered for the duration of the server and will overwrite any other endpoint serving the same path
   + DELETE /test-api/endpoint
    - api path
    - all:
      - if not present, the delete call will simple end the current responses being provided (equivalent to the 'times' counter reaching 0) and have the api path revert to previous responses
        - if an endpoint was set with times = -1 or if it has no default behaviour, it then DELETE will behave just as if `all` was present
      - if present, will remove _all_ the endpoints serving an API path
